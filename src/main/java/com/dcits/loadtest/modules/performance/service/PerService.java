package com.dcits.loadtest.modules.performance.service;

/**
 * 接口类
 * @Author shengyin
 * @Date 2019/9/17 19:35
 * @Version 1.0.0
 */
public interface PerService {

    /**
     * 内存泄漏
     * @author shengyin
     * @date 2019/9/17 19:37
     * @param
     * @return void
     */
    void memoryLeak ();

}
