package com.dcits.loadtest.modules.performance.service.impl;

import com.dcits.loadtest.modules.performance.service.PerService;

import java.util.ArrayList;
import java.util.List;

/**
 * 实现类
 * @Author shengyin
 * @Date 2019/9/17 19:38
 * @Version 1.0.0
 */
public class PerServiceImpl implements PerService {

    @Override
    public void memoryLeak() {
        List<int[]> list = new ArrayList<int[]>();

        Runtime run = Runtime.getRuntime();
        int i=1;
        while(true){
            int[] arr = new int[1024 * 8];
            list.add(arr);

            if(i++ % 1000 == 0 ){
                System.out.print("最大内存=" + run.maxMemory() / 1024 / 1024 + "M,");
                System.out.print("已分配内存=" + run.totalMemory() /1024 / 1024 + "M,");
                System.out.print("剩余空间内存=" + run.freeMemory() / 1024 / 1024 + "M");
                System.out.println("最大可用内存=" + ( run.maxMemory() - run.totalMemory() + run.freeMemory() ) / 1024 / 1024 + "M");
            }
        }
    }
}
