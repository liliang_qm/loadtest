package com.dcits.loadtest.modules.user.controller;

import com.alibaba.druid.util.StringUtils;
import com.dcits.loadtest.common.exception.AppErrorCode;
import com.dcits.loadtest.common.utils.LoadTestUtil;
import com.dcits.loadtest.common.utils.RedisUtil;
import com.dcits.loadtest.common.utils.Result;
import com.dcits.loadtest.modules.user.dto.UpdatePasswordDTO;
import com.dcits.loadtest.modules.user.entity.UserEntity;
import com.dcits.loadtest.modules.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Description
 *
 * @Author WangYJ
 * @Date 2019/09/05 15:54
 * @Version 1.0.0
 */

@RestController
@RequestMapping("/")
@Api(tags = "用户管理")
public class UserController {
    private static List<String> list = new ArrayList<>();


    @Autowired
    private UserService userService;
    @Autowired
    private RedisUtil redisUtil;

    /**
     * 查询指定用户信息
     * @author WangYJ
     * @date 2019-09-06 11:21
     * @param  id
     * @return {@link Result}
     */
    @GetMapping("/user/queryUserInfo")
    @ApiOperation("查询指定用户信息")
    public Result<UserEntity> queryUserInfo(@RequestParam String id) {
        UserEntity user = userService.queryUserInfo(id);

        return new Result<UserEntity>().ok(user);
    }

    /**
     * 修改密码
     * @author WangYJ
     * @date 2019-09-06 15:41
     * @param  dto
     * @return {@link Result}
     */
    @PostMapping("/user/updatePassWord")
    @ApiOperation("修改密码")
    public Result updatePassWord(@RequestBody UpdatePasswordDTO dto) {

        String password = userService.queryUserInfo(dto.getId()).getPassword();

        //原密码不正确
        if (!password.equals(dto.getPassword())) {
            return new Result().error(AppErrorCode.PASSWORD_ERROR);
        }

        //新旧密码一致
        if (password.equals(dto.getNewPassword())) {
            return new Result().error("新旧密码一致");
        }

        userService.updatePassWord(dto);

        return new Result();
    }

    /**
     * 保存用户信息
     * @author WangYJ
     * @date 2019-09-11 10:55
     * @param  userEntity
     * @return {@link Result}
     */
    @PostMapping("/user/saveUserInfo")
    @ApiOperation("保存用户信息")
    public Result saveUserInfo(@RequestBody UserEntity userEntity) {
        userService.saveUserInfo(userEntity);
        return new Result();
    }

    @GetMapping("/util/cpu")
    @ApiOperation("消耗cpu")
    public Result cpu(@RequestParam Long second) {
        if (second == null) {
            second = 1L;
        }

        second = second * 1000;
        int num = 0;

        Long start = System.currentTimeMillis();
        while (System.currentTimeMillis() - start < second) {
            num++;

            if (num >= Integer.MAX_VALUE) {
                num = 0;
            }
        }


        return new Result();
    }


    @GetMapping("/util/memory")
    @ApiOperation("消耗内存")
    public Result memory(@RequestParam Integer mb, @RequestParam(defaultValue = "true", required = false) boolean enabledGc) {
        String ss = "dcitsdcits";
        if (mb == null) {
            mb = 1;
        }

        Long byteCount = mb * 1024L * 1024;

        while (ss.length() < byteCount) {
            ss = ss + ss;
        }

        if (!enabledGc) {
            list.add(ss);
        }

        return new Result();
    }


    @GetMapping("/util/gcMemory")
    @ApiOperation("允许GC")
    public Result gcMemory() {
        list = new ArrayList<>();
        return new Result();
    }


    @GetMapping("/util/queue")
    @ApiOperation("消耗队列")
    public Result queue(@RequestParam Integer sleepSecond) {
        if (sleepSecond == null) {
            sleepSecond = 0;
        }
        sleepSecond = sleepSecond * 1000;

        Integer i = LoadTestUtil.getLock();

        try {
            Thread.sleep(sleepSecond);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        LoadTestUtil.putLock(i);

        return new Result();
    }

    /**
     *
     * @author xuwangcheng
     * @date 2019/9/18 15:04
     * @param keyName keyName
     * @return {@link Result}
     */
    @GetMapping("/redis/get")
    @ApiOperation("获取redis值")
    public Result<String> redisGet (@RequestParam String keyName) {
        String result = null;
        if (redisUtil.hasKey(keyName)) {
            result = redisUtil.get(keyName).toString();
        }
        return new Result<String>().ok(result);
    }


    @GetMapping("/redis/set")
    @ApiOperation("redis插入值")
    public Result redisSet (@RequestParam String key, @RequestParam String val) {
        redisUtil.set(key, val);

        return new Result();
    }

    @GetMapping("/redis/batchSet")
    @ApiOperation("redis插入值")
    public Result redisBatchSet (@ApiParam(value = "key的基础名称") @RequestParam String baseName, @ApiParam(value = "批量插入条数，默认1000") @RequestParam Long size, @ApiParam(value = "超时秒数，传入负数表示永不过期") @RequestParam int expireTimeSecond) {
        if (size == null) {
            size = 1000L;
        }
        if (StringUtils.isEmpty(baseName)) {
            baseName = "peixun";
        }

        for (long i = 0 ; i < size ; i++) {
            redisUtil.set(baseName + i, "" + i, expireTimeSecond);
        }

        return new Result();
    }

    @GetMapping("/redis/clearAll")
    @ApiOperation("清空redis")
    public Result redisClear () {
        redisUtil.delAll();
        return new Result();
    }

}
