package com.dcits.loadtest.modules.user.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 修改密码入参
 *
 * @Author WangYJ
 * @Date 2019/09/06 11:27
 * @Version 1.0.0
 */

@Data
@ApiModel(value = "修改密码入参")
public class UpdatePasswordDTO {

    @ApiModelProperty(value = "用户Id")
    @NotBlank(message="用户Id不能为空")
    private String Id;

    @ApiModelProperty(value = "原密码")
    @NotBlank(message="原密码不能为空")
    private String password;

    @ApiModelProperty(value = "新密码")
    @NotBlank(message="新密码不能为空")
    private String newPassword;
}
