package com.dcits.loadtest.modules.user.dao;

import com.dcits.loadtest.modules.user.dto.UpdatePasswordDTO;
import com.dcits.loadtest.modules.user.entity.UserEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * 用户管理
 *
 * @Author WangYJ
 * @Date 2019/09/05 15:53
 * @Version 1.0.0
 */

@Mapper
public interface UserDao {

    /**
     * 查询指定用户信息
     * @author WangYJ
     * @date 2019-09-05 16:38
     * @param  id
     * @return {@link UserEntity}
     */
    UserEntity queryUserInfo(@Param("id") String id);

    /**
     * 修改密码
     * @author WangYJ
     * @date 2019-09-11 11:33
     * @param  dto
     * @return {void}
     */
    void updatePassword(@Param("pm") UpdatePasswordDTO dto);

    /**
     * 保存用户信息
     * @author WangYJ
     * @date 2019-09-11 11:33
     * @param  userEntity
     * @return {void}
     */
    void saveUserInfo(@Param("pm") UserEntity userEntity);
}
