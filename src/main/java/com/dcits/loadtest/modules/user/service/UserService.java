package com.dcits.loadtest.modules.user.service;

import com.dcits.loadtest.modules.user.dto.UpdatePasswordDTO;
import com.dcits.loadtest.modules.user.entity.UserEntity;

/**
 * 用户信息
 *
 * @Author WangYJ
 * @Date 2019/09/05 15:52
 * @Version 1.0.0
 */

public interface UserService {

    /**
     * 查询指定用户信息
     * @author WangYJ
     * @date 2019-09-06 15:25
     * @param  id
     * @return {@link UserEntity}
     */
    UserEntity queryUserInfo(String id);

    /**
     * 修改密码
     * @author WangYJ
     * @date 2019-09-06 15:26
     * @param  dto
     * @return {void}
     */
    void updatePassWord(UpdatePasswordDTO dto);

    void saveUserInfo(UserEntity userEntity);
}
