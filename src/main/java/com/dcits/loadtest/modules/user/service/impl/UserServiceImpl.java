package com.dcits.loadtest.modules.user.service.impl;

import com.dcits.loadtest.modules.user.dao.UserDao;
import com.dcits.loadtest.modules.user.dto.UpdatePasswordDTO;
import com.dcits.loadtest.modules.user.entity.UserEntity;
import com.dcits.loadtest.modules.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 用户管理
 *
 * @Author WangYJ
 * @Date 2019/09/05 15:54
 * @Version 1.0.0
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserEntity queryUserInfo(String id) {
        return userDao.queryUserInfo(id);
    }

    @Override
    public void updatePassWord(UpdatePasswordDTO dto) {
        userDao.updatePassword(dto);
    }

    @Override
    public void saveUserInfo(UserEntity userEntity) {
        userDao.saveUserInfo(userEntity);
    }
}
