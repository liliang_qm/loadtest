package com.dcits.loadtest.common.exception;

/**
 * 错误编码
 *
 * @Author WangYJ
 * @Date 2019/09/05 16:23
 * @Version 1.0.0
 */
public enum AppErrorCode {
    INTERNAL_SERVER_ERROR(500, "系统内部错误"),
    UNAUTHORIZED(401, "无权操作"),
    FORBIDDEN(403, "禁止访问"),
    INVALID_PARAMETER(800, "参数校验失败"),
    DATABASE_EXCEPTION(9999, "数据库操作异常"),

    //公共
    NOT_NULL(10001, "{0}不能为空"),
    DB_RECORD_EXISTS(10002, "数据库已存在该记录"),
    PARAMS_GET_ERROR(10003, "参数获取失败"),
    ACCOUNT_PASSWORD_ERROR(10004, "账号密码错误"),
    ACCOUNT_DISABLE(10005, "账户已停用"),
    IDENTIFIER_NOT_NULL(10006, "唯一标识不能为空"),
    CAPTCHA_ERROR(10007, "验证码不正确"),
    SUB_MENU_EXIST(10008, "请先删除子菜单或按钮"),
    PASSWORD_ERROR(10009, "原密码不正确"),
    ACCOUNT_NOT_EXIST(10010, "账号不存在"),
    SUPERIOR_DEPT_ERROR(10010, "上级部门选择错误"),
    SUPERIOR_MENU_ERROR(10011, "上级菜单不能为自身"),
    DATA_SCOPE_PARAMS_ERROR(10012, "数据权限接口，只能是Map类型参数"),
    DEPT_SUB_DELETE_ERROR(10013, "请先删除下级部门"),
    DEPT_USER_DELETE_ERROR(10014, "请先删除部门下的用户"),
    ACT_DEPLOY_ERROR(10015, "部署失败，没有流程"),
    ACT_MODEL_IMG_ERROR(10016, "模型图不正确，请检查"),
    ACT_MODEL_EXPORT_ERROR(10017, "模型导出失败"),
    UPLOAD_FILE_EMPTY(10018, "请上传文件"),
    TOKEN_NOT_EMPTY(10019, "token不能为空"),
    TOKEN_INVALID(10020, "token失效，请重新登录"),
    ACCOUNT_LOCK(10021, "账号已被锁定"),
    ACT_DEPLOY_FORMAT_ERROR(10022, "请上传zip、bar、bpmn、bpmn20.xml格式文件"),
    REDIS_ERROR(10023, "Redis服务异常"),
    INVALID_SYMBOL(10024, "不能包含非法字符"),
    JSON_FORMAT_ERROR(10025, "参数格式不正确，请使用JSON格式"),
    FILE_NOT_EXIST(10026, "文件不存在"),
    SAVE_FILE_ERROR(10027, "保存文件出错"),
    PREFIX_TYPE_NOT_EXIST(10028, "编号类型不存在"),
    BUSI_SEQ_NOT_CORRECT(10029, "业务编号不合法"),
    BUSI_OPERATION_NO_PERMISSION(10030, "你没有权限进行此操作！[{}]"),

    //需求相关
    REQUIRE_INFO_NOT_EXIST(20001, "需求不存在"),
    REQUIRE_IS_FINISH(20002, "需求已完成，不允许此操作"),

    //项目相关
    PROJECT_ROOT_FOLDER_DELETE_ERROR(30001, "不能删除根节点"),
    PROJECT_FOLDER_DELETE_ERROR(30002, "请先删除或者转移节点下的测试任务"),
    PROJECT_SUPERIOR_FOLDER_ERROR(30003, "目录父节点不能为自身"),
    PROJECT_SUPERIOR_FOLDER_NOT_EXIST(30004, "目录父节点不存在"),
    PROJECT_FOLDER_LEVEL_ERROR(30005, "目录节点层级设置不正确"),
    PROJECT_FOLDER_NOT_EXIST(30006, "节点目录不存在"),
    PROJECT_FOLDER_DELETE_EXIST_BUG(30007, "目录下存在缺陷，请先删除"),
    PROJECT_FOLDER_NO_PERMISSION(30008, "无权限！只能由负责人（创建人）操作"),
    PROJECT_INFO_NOT_EXIST(30009, "项目不存在"),
    PROJECT_IS_FINISH(30010, "项目已完成，不允许此操作"),

    //任务单相关
    TEST_TASK_NOT_EXIST(40001, "任务单不存在"),
    TEST_TASK_REVIEW_STATUS_ERROR(40002, "评审状态不正确"),
    TEST_TASK_REVIEW_VERSION_NOT_EXIST(40003, "当前不存在评审版本"),
    TEST_TASK_REVIEW_VERSION_ERROR(40004, "评审版本不对应"),
    TEST_TASK_DELETE_EXIST_BUG(40005, "任务单下存在缺陷, 请先删除"),
    TEST_TASK_TRANSFER_PERMISSION_ERROR(40006, "无权转派该测试任务单"),
    TEST_TASK_TRANSFER_SELF_ERROR(40007, "测试任务不能转派给当前执行人"),
    TEST_TASK_CLOSE_REVIEW_STATUS_ERROR(40008, "请先完成评审再关闭任务！"),
    TEST_TASK_NO_REQUIRE_REVIEW(40009, "当前测试任务不需要评审"),
    TEST_TASK_NO_PERMISSION(40010, "无权限！任务单删除/编辑只能由任务创建人操作"),
    TEST_TASK_HAS_BEEN_CLOSED(40011, "任务单已经关闭"),
    TEST_TASK_NO_PERMISSION_CREATE(40012, "无权限！(任务单只能由项目/需求的测试负责人创建)"),
    TEST_TASK_NO_ANY_CASE(40014, "任务单下还没有测试用例"),
    TEST_TASK_NOT_EXEC_CASE_AND_NOT_REPAIR_BUG(40013, "任务下有未执行的用例或未关闭的缺陷，请确认是否继续关闭？"),
    TEST_TASK_NO_PERMISSION_TO_CLOSE_TASK(40015, "你无权关闭此任务单"),
    TEST_TASK_NO_PERMISSION_TO_REVIEW(40016, "你没有评审该任务单的权限"),

    //测试用例
    TEST_CASE_NOT_EXIST(50001, "测试用例不存在"),
    TEST_CASE_DELETE_EXIST_BUG(50002, "测试用例下存在缺陷, 请先删除"),
    TEST_CASE_REVIEW_STATUS_ERROR(50003, "用例必须评审通过才能继续执行"),
    TEST_CASE_HAS_REVIEW_NO_PASS_COUNT(50004, "该任务下有评审不通过的测试用例"),
    TEST_CASE_UPDATE_ERROR_BY_REVIEWING(50005, "评审中不允许修改用例信息"),
    TEST_CASE_CANNOT_EXEC_BY_TASK_CLOSED(50006, "任务已关闭不能执行用例"),

    //缺陷
    BUG_CREATE_ERROR_BY_CASE_NO_RUN(60001, "用例未执行，不允许创建缺陷"),
    BUG_CREATE_ERROR_BY_CASE_RESULT_NOT_FAIL(60002, "用例执行未通过时才能创建缺陷"),
    BUG_INFO_NOT_EXIST(60003, "该缺陷不存在"),
    BUG_INFO_IS_CLOSED(60004, "该缺陷已关闭"),
    BUG_INFO_HAS_BEEN_IN_FLOW(60005, "该缺陷已进入处理流程"),


    //统计报表和报告
    REPORT_WORD_CREATE_EXCEPTION(70001, "测试报告生成出错,请稍后再试"),
    REPORT_CREATE_NO_DATA_ERROR(70002, "没有足够的测试数据以生成报告"),

    //流程相关
    FLOW_HANDLER_NOT_EXIST(80001, "不存在的处理流程"),
    FLOW_HANDLE_STATUS_ERROR(80002, "当前流程状态下不允许此操作"),
    FLOW_HANDLE_USER_ERROR(80003, "当前用户无法处理该流程"),
    FLOW_HANDLE_LACK_NEXT_USER_ID(80004, "请选择转派/移交的用户"),

    //测试用例库相关
    CASE_LIBRARY_PARENT_NODE_NOT_EXIST(90001, "父节点不存在"),
    CASE_LIBRARY_NODE_NOT_EXIST(90002, "节点不存在"),
    CASE_LIBRARY_COPY_NODE_PID_ERROR(90003, "不能复制到指定的父节点"),
    CASE_LIBRARY_NODE_IS_FOLDER_TYPE(90004, "当前节点为目录,不允许操作"),
    CASE_LIBRARY_NODE_IS_NODE_TYPE(90005, "当前节点为不是目录，不允许操作"),
    CASE_LIBRARY_PARENT_NODE_IS_MUST_FOLDER(90006, "父节点必须为目录"),

    //产品模块
    PRODUCT_MODULE_NODE_NOT_EXIST(100005, "节点不存在"),
    PRODUCT_MODULE_PARENT_NODE_NOT_EXIST(100006, "父节点不存在"),
    PRODUCT_MODULE_NODE_IS_MUST_FOLDER(100007, "父节点必须为目录")
    ;

    private int code;
    private String msg;

    private AppErrorCode(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public static AppErrorCode getByCode(int code) {
        for (AppErrorCode c:values()) {
            if (code == c.getCode()) {
                return c;
            }
        }
        return null;
    }
}
